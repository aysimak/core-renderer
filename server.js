const http = require('http');
const express = require('express');
var bodyParser = require('body-parser')

const itemsRouter = require('./routes/router');

global.imageSystemPath = "C:/Users/47939/Documents/Repos/images/"
global.excelSystemPath = "C:/Users/47939/Documents/Repos/Input/"
global.projectSystemPath = "C:/Users/47939/Documents/Repos/Projects/"
global.renderOutputSystemPath = "/Users/47939/Documents/Repos/Projects/output/"
global.excelOutputSystemPath = "C:/Users/47939/Documents/Repos/Projects/ExcelOutput/"
global.renderQueueSize = 4
global.renderQueueWaitingTime = 180000; // 5x60x1000 milliseconds

const app = express();
app.use(bodyParser.json({}));

app.use(express.json());

app.use('/renderall', itemsRouter);
app.use('/render', itemsRouter);
app.use('/', itemsRouter);
app.use('/parse', itemsRouter);

const server = http.createServer(app);

const port = 8000;

server.listen(port);

console.debug('Server listening on port ' + port);

module.exports=app;