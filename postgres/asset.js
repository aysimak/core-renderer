const conn = require('./connection')

const uuidv1 = require('uuid/v1')

module.exports = {
  selectAssetsByProject: function (projectId) {
    conn.pool.query('SELECT * FROM public.asset where projectId = ' + projectId  + ' ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      return results.rows;
    })
  },
  insertAsset: function (name, status, path, projectId) {
    conn.pool.query('INSERT INTO public.asset (id, name, status, path, "projectId") VALUES ($1, $2, $3, $4, $5)', [uuidv1(), name, status, path, projectId], (error, results) => {
      if (error) {
        throw error
      }
      return results.insertId;
    })
  }
}
