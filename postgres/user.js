const conn = require('./connection')

const uuidv1 = require('uuid/v1')

module.exports = {
  selectUsers: function () {
    conn.pool.query('SELECT * FROM public.user ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      return results.rows;
    })
  },
  insertUsers: function (username, organisation, email, phone, address) {
    conn.pool.query('INSERT INTO public.user (id, username, organisation, email, phone, address) VALUES ($1, $2, $3, $4, $5, $6)', [uuidv1(), username, organisation, email, phone, address], (error, results) => {
      if (error) {
        throw error
      }
      return results.insertId;
    })
  }
}
