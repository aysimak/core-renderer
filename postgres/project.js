const conn = require('./connection')

const uuidv1 = require('uuid/v1')

module.exports = {
  selectProjectsById: function (projectId) {
    conn.pool.query('SELECT * FROM public.project where id = ' + projectId  + ' ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      return results.rows;
    })
  },
  selectProjectsByUserId: function (userId) {
    conn.pool.query('SELECT * FROM public.project where userId = ' + userId  + ' ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      return results.rows;
    })
  },
  insertProject: function (name, userId, path) {
    conn.pool.query('INSERT INTO public.project (id, name, "userId", path) VALUES ($1, $2, $3, $4)', [uuidv1(), name, userId, path], (error, results) => {
      if (error) {
        throw error
      }
      return results.insertId;
    })
  }
}
