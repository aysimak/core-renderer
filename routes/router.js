const express = require('express');
const aepx = require('../service/json')
const image = require('../service/image')
const removebg = require('../service/removebg')
const excel = require('../service/excel')
const fs = require('fs')
const router = express.Router();

const postgre = require('../postgres/user')

const renderService = require('../service/render')
const assetService = require('../service/asset')
const projectService = require('../service/project')
const userService = require('../service/user')

const multer = require('multer');
const upload = multer({ dest: 'uploads/' });

router.post('/user/add', (req, res) => {
  userService.addUser(req.body.userName, req.body.organisation, req.body.email, req.body.phone, req.body.address, res)
});

router.post('/upload', upload.single('project'), (req, res) => {
  projectService.addProject(res, req.body.name, req.body.userId, req.file.path);
});

router.post('/upload', upload.single('image'), (req, res) => {
  assetService.addAsset(res, req.body.name, req.body.status, req.file.path, req.body.projectId);
});

router.post('/parse', function (req, res) {
  return res.send(aepx.parseToJson(req.body.path));
});

router.post('/parseLayersForComposition', function (req, res) {
  return res.send(aepx.getLayersFromComposition(req.body.path, req.body.compName));
});

router.post('/prepareExcel', function (req, res) {
  return res.send(excel.prepareExcel(req.body.path));
});

router.post('/prepareExcelForComp', function (req, res) {
  return res.send(excel.prepareExcelForComp(req.body.path, req.body.compName));
});

router.post('/renderall', (req, res) => {
  var jsonArr = req.body;
  for (var i in jsonArr) {
    renderJson(jsonArr[i]).catch(console.error);
  }
  return res.send('Render has successfully started');
});

router.post('/render', (req, res) => {
  renderService.render(req, res);
});

router.post('/renderInExcel', (req, res) => {
  renderService.renderInExcel(req, res);
});

router.post('/resizeAndRemoveBG', function (req, res) {
   image.resizeAndRemoveBG(req.body.image, req.body.width, req.body.height, req.body.footage);
   res.send('resize api works');
});

router.post('/resize', function (req, res) {
  image.resize(req.body.image, req.body.width, req.body.height, req.body.footage);
  res.send('resize api works');
});

module.exports = router;