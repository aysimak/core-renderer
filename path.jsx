function isSourceLayered(av_item) {
    // check if there is a "/"
    if (av_item.name.indexOf("/") != -1) {
        return true;
    }

    return false;
}

function canImportAsComp(file) {
    var io = new ImportOptions(file);

    return io.canImportAs(ImportAsType.COMP);
}

function getPathFromItem(item) {
    if (item.mainSource && item.mainSource.file) {
        return item.mainSource.file.path + "/" + item.mainSource.file.name;
    } else {
        return false;
    }
}

function getReplacePath(path) {
    var matches = path.match(regex);

    if (matches && matches.length) {
        var splitPath = path.split(regex);
        if (splitPath && splitPath.length > 1) {
            var newPath = (NX.projectFolder + matches[matches.length - 1] + splitPath[1]).replace(/\//g, '\\');

            return newPath;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

var regex = /\/\(\w+\)\//;

for (var i = 1; i <= app.project.items.length; i++) {
    var item = app.project.items[i];
    gAECommandLineRenderer.log_file.writeln("ITEM: " + item.name + "[" + item.toString() + "]");

    var path = getPathFromItem(item);
    if (path != false) {
        var replacePath = getReplacePath(path);

        if (replacePath != false) {
            var file = new File(replacePath);

            if (!file.exists) {
                gAECommandLineRenderer.log_file.writeln("ERROR: could not make file from " + replacePath);    
            } else {
                if (!isSourceLayered(item) || (isSourceLayered(item) && !canImportAsComp(file))) {
                    gAECommandLineRenderer.log_file.writeln("INFO: Pointing " + path + " to " + replacePath);
                    item.replace(file); // This works fine
                } else if (isSourceLayered(item) && canImportAsComp(file)) {
                    gAECommandLineRenderer.log_file.writeln("INFO: Pointing layered " + path + " to " + replacePath);
                    item.replace(file); // This shows all contents of the PSD instead of the layer
                }
            }
        } else {
            gAECommandLineRenderer.log_file.writeln("NO REPLACE: " + path);
        }
    }

    gAECommandLineRenderer.log_file.writeln("END ITEM \n");
}