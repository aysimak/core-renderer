const { init, render } = require('@nexrender/core')
const parser = require('../service/json')
const imageFormatter = require('../service/image')
var xlsx = require('node-xlsx');
var fs = require('fs');

const settings = init({
    logger: console
})

var resultExcelForRender;
var processedRows;
var totalRows;

module.exports = {
    renderInExcel: async function (req, response) {
        try {
          renderFromExcel(req); 
        } catch (error) {
            console.log("General error in rendering : " + error);
            saveResultExcel();
        } 
        return response.send('Render has successfully started');
    },
    render: function (req, response) {
        renderSet(req.body.projectName, req.body.compName, req.body.projectPath, req.body.assets);
        return response.send('Render has successfully started');
    }
}

function renderFromExcel(req) {
    var obj = xlsx.parse(excelSystemPath + req.body.excelPath);

    obj.forEach(async function (sheet) {
        var numberOfRows = numberOfNonEmptyRows(sheet);
        cleanVariables(numberOfRows - 1);
        var layerNames = getLayerNames(sheet);
        var firstRow = [];
        firstRow.push('Video Name');
        firstRow.push('Comp Name');
        firstRow.push(...layerNames);
        resultExcelForRender.push(firstRow);
        var lastRenderedRow = 1;
        lastRenderedRow = await resumeRender(lastRenderedRow, numberOfRows, sheet, layerNames, req);
        var interval = setInterval(async function () {
            if (interval != null && lastRenderedRow >= numberOfRows) {
                clearInterval(interval);
                console.log("Render queue is finished. Total number of rendered rows are : " + lastRenderedRow);
            } else if (interval != null && processedRows >= lastRenderedRow - 1) {
                console.log("Next render batch is resumed after row : " + lastRenderedRow);
                lastRenderedRow = await resumeRender(lastRenderedRow, numberOfRows, sheet, layerNames, req);
            }
        }, renderQueueWaitingTime);
    });
}

function cleanVariables(rowCount){
    totalRows = rowCount;
    processedRows = 0;
    resultExcelForRender = [];
}

async function resumeRender(lastRenderedRow, numberOfRows, sheet, layerNames, req) { 
    var endIndex = (lastRenderedRow + renderQueueSize) > numberOfRows ? numberOfRows : (lastRenderedRow + renderQueueSize);
    for (var rowNumber = lastRenderedRow; rowNumber < endIndex; rowNumber++) {
        try{
            await renderRow(sheet.data[rowNumber], layerNames, req, rowNumber);
        }catch(error) {
            console.error("Render failed for the row : " + rowNumber + " with error: " + error);
        }
        lastRenderedRow++;
    }
    return lastRenderedRow;
}

function getLayerNames(sheet) {
    var layerNames = [];
    var column = 0;
    sheet.data[0].forEach(function (layerName) {
        if (column > 1) {
            layerNames.push(layerName);
        }
        column++;
    });
    return layerNames;
}

function numberOfNonEmptyRows(sheet) {
    var rows = 0;
    sheet.data.forEach(async function(row) {
        if((row[0] != null) && (row[0] != "")) {
            rows++; 
        }
    });
    return rows;
}
 
async function renderRow(row, layerNames, req, renderIndex) {
    if (row.length != 0) {
            var assetsJson = [];
            var videoName = row[0];
            var compName = row[1];
            var outputRow = [];
            await row.forEach(async function (layerValue, column) {
                outputRow.push(layerValue);
                if (column <= layerNames.length + 1 && column > 1 && layerValue != undefined && layerValue != "") {
                    if(isImage(layerValue)) {
                        var footagePath = getFootagePath(req.body.projectPath);
                        var imageJson = await prepareImageJson(layerValue, layerNames[column - 2], footagePath);
                        assetsJson.push(imageJson);
                    }else if(isVideo(layerValue)) {
                        var videoJson = prepareVideoJson(layerValue, layerNames[column - 2]);
                        assetsJson.push(videoJson);
                    }else if(isAudio(layerValue)) {
                        var videoJson = prepareAudioJson(layerValue, layerNames[column - 2]);
                        assetsJson.push(videoJson);
                    }else {
                        assetsJson.push({
                            "type": "data",
                            "layerName": layerNames[column - 2],
                            "property": "Source Text",
                            "value": layerValue
                        });
                    }
                }

                if (assetsJson.length == layerNames.length) {
                    renderSet(videoName, compName, req.body.projectPath, assetsJson, outputRow);
                }
            });
            
    }
}

// TODO Change http check here
function isImage(layerValue) {
    return layerValue.includes('.png') || layerValue.includes('.jpeg') || layerValue.includes('.jpg') || layerValue.includes('gif') || layerValue.includes('http');
} 

function isVideo(layerValue) {
    return layerValue.includes('.mp4') || layerValue.includes('.mpeg') || layerValue.includes('.mov');
} 

function isAudio(layerValue) {
    return layerValue.includes('.mp3') || layerValue.includes('.m4a');
} 

function isResizeAsked(value) {
    return value.includes('?resize') || value.includes('resize=');
} 

function isBgRemoveAsked(value) {
    return value.includes('bgRemove');
} 

function isUrl(value) {
    return value.includes('http') || value.includes('www');
} 

function removeRemoveBg(value) {
    if(value.includes('&bgRemove')) {
        return value.replace('&bgRemove','');
    }    
    if(value.includes('bgRemove')) {
        return value.replace('bgRemove','');
    }  
    return value;
} 

function buildPath(value) {
    var occurance = (value.match(/\//g) || []).length;
   return value.split(value.split('/')[occurance])[0];
} 

function getOriginalPathIfExists(layerField, footagePath) {
    if(!layerField.includes('?resize')){
        console.log("Layer name doesnt include ?resize " + layerField);
        return "";
    }
    if(layerField.includes('?resize=')){
        return footagePath + layerField.split('?resize=')[1];
    }
    if(layerField.includes('&bgRemove=')){
        return footagePath + layerField.split('&bgRemove=')[1];
    }
    if(layerField.includes('?resize&bgRemove')){
        return footagePath + layerField.split('?resize&bgRemove')[0];
    }
    if(layerField.includes('?resize')){
        return footagePath + layerField.split('?resize')[0];
    }
    return footagePath + layerField;
} 

function getFootagePath(projectPath) {
    return projectSystemPath + buildPath(projectPath) + '(Footage)/';
} 

function prepareVideoJson(layerValue, layerName) {
    return {
        "src": "file:///" + projectSystemPath + layerValue,
        "type": "video",
        "layerName": layerName
    };
}

function prepareAudioJson(layerValue, layerName) {
    return {
        "src": "file:///" + projectSystemPath + layerValue,
        "type": "audio",
        "layerName": layerName
    };
}

async function prepareImageJson(layerValue, layerName, footagePath) {
    var resizeForAll = isResizeAsked(layerName);
    var height = undefined;
    var width = undefined;
    if(resizeForAll) {
        var originalImage = getOriginalPathIfExists(layerName, footagePath);
        var dimension = imageFormatter.getSize(originalImage);
        width = dimension.width;
        height = dimension.height;
        resizeForAll = true;
    } 
    if (resizeForAll) {
        try {
            var finalImage = "";
            if(isBgRemoveAsked(layerName)){
                finalImage = await imageFormatter.resizeAndRemoveBG(layerValue, width, height, footagePath);
                console.log("Image is resized and bgremoved : " + finalImage);
            }else{
                finalImage = await imageFormatter.resize(layerValue, width, height, footagePath);
                console.log("Image is resized : " + finalImage);
            }
            if(finalImage != ""){
                return {
                    "src": "file:///" + imageSystemPath + finalImage,
                    "type": "image",
                    "layerName": layerName.split('?resize')[0]
                };
            }
        }catch (error) {
            console.log(error);
        }
    }else if (isResizeAsked(layerValue)) {
        try {
            var finalImage = "";
            var dimensions = layerValue.split('resize=')[1].split('x');
            if(isBgRemoveAsked(layerValue)){
                finalImage = await imageFormatter.resizeAndRemoveBG(layerValue.split('?bgRemove')[0], parseInt(dimensions[0], 10), parseInt(dimensions[1], 10), footagePath);
                console.log("Image is resized and bgremoved : " + finalImage);
            }else{
                finalImage = await imageFormatter.resize(layerValue.split('?resize')[0], parseInt(dimensions[0], 10), parseInt(dimensions[1], 10), footagePath);
                console.log("Image is resized : " + finalImage);
            }
            if(finalImage != ""){
                return {
                    "src": "file:///" + imageSystemPath + finalImage,
                    "type": "image",
                    "layerName": layerName
                };
            }
        }catch (error) {
            console.log(error);
        }
    }else if (isUrl(layerValue)) {
        return {
            "src": layerValue,
            "type": "image",
            "layerName": layerName
        };
    }else {
        return {
            "src": "file:///" + projectSystemPath + layerValue,
            "type": "image",
            "layerName": layerName
        };
    }
} 

function renderSet(videoName, compName, projectPath, assets, outputRow) {
    var path = projectSystemPath + projectPath;
    if(compName == undefined || compName == ""){
        template = parser.parseToJson(path + "x");
        if(template == undefined || template.compNames == undefined){
            return response.send('Error in parsing the project');
        }
        compName = template.compNames[0];
    }

    var json = {}
    json["template"] = {
        "src": "file:///" + path,
        "composition": compName
    };

    json["actions"] = {
        "postrender": [
            {
                "module": "@nexrender/action-encode",
                "preset": "mp4",
                "output": "encoded.mp4"
            },
            {
                "module": "@nexrender/action-copy",
                "input": "encoded.mp4",
                "output": renderOutputSystemPath + videoName + ".mp4"
            }
        ]
    }

    if(assets != undefined && assets.length > 0){
        json["assets"] = assets;
    }
    
    render(json, settings).then(
        () => {
            saveResultForRender(outputRow, 'Row has been rendered successfully');
        }
    ).catch((error) => {
        console.error(error);
        saveResultForRender(outputRow, error);
    });
}

function saveResultForRender(outputRow, result) {
    try{
        processedRows++;
        if(outputRow != undefined){
            outputRow.push(result);
            resultExcelForRender.push(outputRow);
            if (processedRows == totalRows) {
                saveResultExcel();
            }
        }
    }catch(ex){
        console.error(ex);
    }
}

function saveResultExcel() {
    var buffer = xlsx.build([{name: "resultExcel", data: resultExcelForRender}]);
    var excelName = excelOutputSystemPath + "result" + Math.random() + ".xlsx";
    fs.writeFile(excelName, buffer, err => {
        if (err) {
            console.error(err);
        } else {
            console.log("Result excel is saved as: " + excelName);
        }
    });
}

