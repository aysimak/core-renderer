const db = require('../postgres/project')

var replaceExt = require('replace-ext');
var fs = require('fs');

module.exports = {
    addProject: function (response, name, userId, path) {
     response.status(201).send(`Project added with ID:` + db.insertProject(name, userId, path));
    },
    convertAEPToAEPX: function (response, path) {
      var newPath = replaceExt(path, '.aepx');
 
      fs.rename(newPath, path, function (err) {
        if (err) throw err;
        console.log('File Renamed.');
      });
      
      response.status(200).send(`Project convert at the path:` + newPath);
    }
}