const sharp = require('sharp');
const axios = require('axios');
var fs = require('fs');
const Promise = require('bluebird');
const removebg = require('../service/removebg')
var request = require('request');
const Jimp = require('jimp');

const apiKey = "qoofmD8uVXSxveHYV7n75zHd";

var sizeOf = require('image-size');

module.exports = {
    resizeAndRemoveBG: async function (image, width, height, footagePath) {
        var imageNamePathArr = [];
        if(image.includes('\\')) {
            imageNamePathArr = image.split('\\');
        } else if(image.includes('/')) {
            imageNamePathArr = image.split('/');
        } else {
            imageNamePathArr.push(image);
        }  
        var imageName = imageNamePathArr[imageNamePathArr.length - 1];

        var imageNameArr = imageName.split('.');
        var resizedImage = imageNameArr[0] + "JustResized." + imageNameArr[1];
        var newBGRemovedImage = imageNameArr[0] + "Final.png";
        
        var imageData = await getImageData(image, footagePath);
        try {
            await resize(imageData, resizedImage, width, height);
            console.log("Resize is completed for image: " + imageName + resizedImage);
        } catch (error) {
            console.log(error);
        }

        try {
            await removeBackground(resizedImage, newBGRemovedImage);
            console.log("Image is saved as background removed");
        } catch (error) {
            console.log(error);
        }
        console.info("Image resized and saved as: " + imageSystemPath + newBGRemovedImage);
        return newBGRemovedImage;
    },
    resize: async function (image, width, height, footagePath) {
        var imageNamePathArr = [];
        if(image.includes('\\')) {
            imageNamePathArr = image.split('\\');
        } else if(image.includes('/')) {
            imageNamePathArr = image.split('/');
        } else {
            imageNamePathArr.push(image);
        }       
        var imageName = imageNamePathArr[imageNamePathArr.length - 1];

        var imageNameArr = imageName.split('.');
        var resizedImage = imageNameArr[0] + "Resized." + imageNameArr[1];
        var newBGRemovedImage = imageNameArr[0] + "Final." + imageNameArr[1];
        
        var imageData = await getImageData(image, footagePath);
        try {
            await resize(imageData, resizedImage, width, height);
            console.log("Resize is completed for image: " + imageName + resizedImage);
        } catch (error) {
            console.log(error + "Not able to resize for image: " + imageData);
        }
        console.info("Image resized and saved as: " + imageSystemPath + newBGRemovedImage);
        return resizedImage;
    },
    getSize: function (image) {
        try {
            var dimensions = sizeOf(image);
        } catch (error) {
            console.log(error);
        }
        return dimensions;
    }
};

async function getImageData(image, footagePath) {
    if(image.includes('http') || image.includes('www')) {
        var response = null;
        try {
            response = await getImage(image);
            return response.data;
        } catch (error) {
            console.log(error);
            throw error;
        }
    }else {
        return footagePath + image;
    } 
}

function getImage(url) {
    return axios.get(url, { responseType: 'arraybuffer' })
        .then((response) => {
            return response;
        });
}

async function resize(image, resizedImage, width, height) {
    try{
        const imageR = await Jimp.read(image);
        await imageR.resize(width, height);
        await imageR.writeAsync(imageSystemPath + resizedImage);
    }catch(ex){
        console.log(ex);
    }
}

async function resizeSharp(image, resizedImage, width, height) {
    return sharp(image)
        .resize(parseInt(width), parseInt(height))
        .toFile(imageSystemPath + resizedImage)
        .then(() => {
            return;
        });
}

function resizeNew(path, format, width, height) {
    const readStream = fs.createReadStream(path)
    return readStream
  }

function removeBackground(oldImage, newBGRemovedImage) {
    return new Promise((resolve, reject) => {
        request.post({
            url: 'https://api.remove.bg/v1.0/removebg',
            formData: {
                image_file: fs.createReadStream(imageSystemPath + oldImage),
                size: 'auto',//full
            },
            headers: {
                'Content-Type': 'multipart/form-data',
                'X-Api-Key': apiKey
            },
            encoding: null
        }, (error, response, body) => {
            if (error) {
                console.error('Request failed:', error);
                reject(error);
            }
           /* if (response.statusCode != 200) {
                resolve(body);
            }*/
            fs.writeFileSync(imageSystemPath + newBGRemovedImage, body);
            return resolve(body);
        });
    });
}
