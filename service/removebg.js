const apiKey = "qoofmD8uVXSxveHYV7n75zHd";

var request = require('request');
var fs = require('fs');

module.exports = {
    removeBG: function (path) {
      var imageNameArr = path.split('/');
      var oldImageName = imageNameArr[imageNameArr.length - 1];

      request.post({
        url: 'https://api.remove.bg/v1.0/removebg',
        formData: {
          image_file: fs.createReadStream(path),
          size: 'auto',
        },
        headers: {
          'X-Api-Key': apiKey
        },
        encoding: null
      }, function(error, response, body) {
        if(error) return console.error('Request failed:', error);
        if(response.statusCode != 200) return console.error('Error:', response.statusCode, body.toString('utf8'));
        var oldImageArr = oldImageName.split('.');
        var newImage = oldImageArr[0] + "RemovedBG." + oldImageArr[1];
        //fs.writeFileSync(newImage, body);
        return body;
      });
    }
};

