const db = require('../postgres/asset')

module.exports = {
    addAsset: function (response, name, status, path, projectId) {
      response.status(201).send(`Asset added with ID:` + db.insertAsset(name, status, path, projectId));
    },
    selectAssetsByProject: function (response, projectId) {
      return db.selectAssetsByProject(projectId);
    }
}