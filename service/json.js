const fs = require('fs');
const path = require('path');
const aepx = require('aepx');

const inputPath = '../input';
const outputPath = '../../output';

// TODO remove duplicate values
module.exports = {
    parseToJson: function (path) {
        const aepxJson = aepx.parseFileSync(path);
        const json = JSON.stringify(aepxJson);

        return getLayersFromItems(aepxJson.fold.items, []);
    },
    getLayersFromComposition: function (path, compName) {
      var referenceIds = {
        layerIds : [],
        compIds : []
      };

      const items = aepx.parseFileSync(path).fold.items;
      var compItem = getCompByName(items, compName);
      getReferenceIdsFromComp(compItem, referenceIds);
      
      //var subCompItems = findSubCompItems(referenceIds, aepxJson);  
      while(referenceIds.compIds.length != 0){
        var subCompIds = referenceIds.compIds;
        referenceIds.compIds = [];
        var subComps = [];
        subCompIds.forEach(compId => subComps.push(getCompById(items, compId))); 
        subComps.forEach(compItem => getReferenceIdsFromComp(compItem, referenceIds));
      }

      return getLayersFromItems(items, referenceIds.layerIds);
    }
  };

function getLayersFromItems(items, referenceIds) {
  var result = {
    textLayers: {},
    imageLayers: {},
    videoLayers: {},
    audioLayers: {},
    compNames: []
  };
  items.forEach(function (item) {
    // asset_type 15 is composition
    if (item.idta != undefined && item.idta.asset_type == 15 && item.filename != "" && item.file_reference != "") {
      pushToResults(result.compNames, item, referenceIds);
    }

    if (item.idta != undefined && item.idta.asset_type == 5 && item.filename != "" && item.filename != undefined && item.file_reference != "" && !item.filename.includes('.psd')) {
      pushFileToResults(result.imageLayers, item, referenceIds);
    }

    if (item.idta != undefined && item.idta.asset_type == 3 && item.filename != "" && item.file_reference != "") {
      pushFileToResults(result.videoLayers, item, referenceIds);
    }

    if (item.idta != undefined && item.idta.asset_type == 7 && item.filename != "" && item.file_reference != "") {
      pushFileToResults(result.audioLayers, item, referenceIds);
    }

    // asset_type 2 is folder, then look for items in sfdr
    if (item.idta != undefined && item.idta.asset_type == 2 && item.sfdr != undefined && item.sfdr.items != undefined) {
      item.sfdr.items.forEach(function (subItem) {
        if (subItem.idta != undefined && subItem.idta.asset_type == 15 && result.compNames.indexOf(subItem.string) === -1) {
          pushToResults(result.compNames, subItem, referenceIds);
        }

        if (subItem.idta != undefined && subItem.idta.asset_type == 5 && subItem.filename != "" && item.filename != undefined && subItem.file_reference != "" && !item.filename.includes('.psd')) {
          pushFileToResults(result.imageLayers, subItem, referenceIds);
        }

        if (subItem.idta != undefined && subItem.idta.asset_type == 3 && subItem.filename != "" && subItem.file_reference != "") {
          pushFileToResults(result.videoLayers, subItem, referenceIds);
        }

        if (subItem.idta != undefined && subItem.idta.asset_type == 7 && subItem.filename != "" && subItem.file_reference != "") {
          pushFileToResults(result.audioLayers, subItem, referenceIds);
        }

        pushTextToResults(subItem, result, referenceIds);
      });
    }

    pushTextToResults(item, result, referenceIds);

  });
  return result;
}

function pushTextToResults(subItem, result, referenceIds) {
  if (subItem.layr != undefined) {
    subItem.layr.forEach(function (layer) {
      if ((layer.ldta.type == 1 || layer.ldta.type == 8) && layer.ldta.name != undefined && layer.ldta.name != "") {
        if(isPartOfReferenceIds(layer.ldta.layer_id, referenceIds)){
          result.textLayers[layer.string] = layer.string;
        }
      }
    });
  }
}

function pushToResults(result, item, referenceIds) {
  if(isPartOfReferenceIds(item.idta.id, referenceIds)){
    result.push(item.string);
  }
}

function pushFileToResults(result, item, referenceIds) {
  if(isPartOfReferenceIds(item.idta.id, referenceIds)){
    result[item.filename] = item.file_reference;
  }
}

function isPartOfReferenceIds(itemId, referenceIds){
  return referenceIds == undefined || referenceIds.length == 0 || referenceIds.indexOf(itemId) != -1;
}

function getCompByName(items, compName) {
  var compItem;
  items.forEach(function (item) {
    //find our comp
    if (IsComp(item) && item.string == compName) {
      compItem = item;
    }

    if (item.idta != undefined && item.idta.asset_type == 2 && item.sfdr != undefined && item.sfdr.items != undefined) {
      item.sfdr.items.forEach(function (subItem) {
        if (IsComp(subItem) && subItem.string == compName) {
          compItem = subItem;
        }
      });
    }
  });
  return compItem;
}

function getCompById(items, compId) {
  var compItem;
  items.forEach(function (item) {
    //find our comp
    if (IsComp(item) && item.idta.id == compId) {
      compItem = item;
    }

    if (item.idta != undefined && item.idta.asset_type == 2 && item.sfdr != undefined && item.sfdr.items != undefined) {
      item.sfdr.items.forEach(function (subItem) {
        if (IsComp(subItem) && subItem.idta.id == compId) {
          compItem = subItem;
        }
      });
    }
  });
  return compItem;
}

function IsComp(item) {
  return item.idta != undefined && item.idta.asset_type == 15;
}

function getReferenceIdsFromComp(compositionItem, referenceIds) {
  if (compositionItem.layr != undefined) {
    compositionItem.layr.forEach(function (layerInComp) {
      if (layerInComp.ldta != undefined && layerInComp.ldta.reference_id != undefined && layerInComp.ldta.reference_id != 0) {
        if(layerInComp.ldta.type == 15 && referenceIds.compIds.indexOf(layerInComp.ldta.reference_id) === -1) {
          referenceIds.compIds.push(layerInComp.ldta.reference_id);
        }
        if(layerInComp.ldta.type != 15 && referenceIds.layerIds.indexOf(layerInComp.ldta.reference_id) === -1) {
          //Changed to layer id 
          referenceIds.layerIds.push(layerInComp.ldta.reference_id);
        }
      }else if(layerInComp.ldta != undefined && layerInComp.ldta.reference_id != undefined && layerInComp.ldta.reference_id == 0 ){
        if(layerInComp.ldta.type != 15 && referenceIds.layerIds.indexOf(layerInComp.ldta.reference_id) === -1) {
          referenceIds.layerIds.push(layerInComp.ldta.layer_id);
        }
      }
    });
  }
}


