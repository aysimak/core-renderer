const db = require('../postgres/user')

module.exports = {
    addUser: function (username, organisation, email, phone, address, response) {
      response.status(201).send(`User added with ID:` + db.insertUsers(username, organisation, email, phone, address));
    }
}