const fs = require('fs');
var xlsx = require('node-xlsx');

const parser = require('../service/json')

// TODO remove duplicate values
module.exports = {
    prepareExcel: function (path) {
      var json = parser.parseToJson(path);
      writeExcel(json);
    },
    prepareExcelForComp: function (path, compName) {
      var json = parser.getLayersFromComposition(path, compName);
      writeExcel(json, compName);
    }
  };

function writeExcel(json, compName) {
  const data = [];
  const headers = [];
  const row = [];

  headers.push("Video Name");
  headers.push("Comp Name");

  row.push("sample_name");
  
  if(compName != undefined){
    row.push(compName);
  }else{
    row.push(json.compNames[0]);
  }
  
  Object.keys(json.textLayers).forEach(function (layerName) {
    headers.push(layerName);
    row.push(json.textLayers[layerName]);
  });

  Object.keys(json.imageLayers).forEach(function (layerName) {
    pushtoRow(headers, layerName, row, json.imageLayers);
  });

  Object.keys(json.videoLayers).forEach(function (layerName) {
    pushtoRow(headers, layerName, row, json.videoLayers);
  });

  Object.keys(json.audioLayers).forEach(function (layerName) {
    pushtoRow(headers, layerName, row, json.audioLayers);
  });

  data.push(headers);
  data.push(row);
  const buffer = xlsx.build([{ name: "furbiSampleRender", data: data }]);
  fs.writeFile(excelOutputSystemPath + 'render' + json.compNames[0] + '.xlsx', buffer, (err) => {
    if (err)
      throw err;
    console.log('Done...');
  });
}
function pushtoRow(headers, layerName, row, layers) {
  headers.push(layerName);
  row.push(layers[layerName].replace(/\\/g, "/").replace(projectSystemPath, ""));
}

